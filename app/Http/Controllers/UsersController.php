<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Requests\Users\UpdateProfileRequest;

use Illuminate\Http\Request;

class UsersController extends Controller
{
/**
 * 
 * Handle users
 * 
 */
    public function index(){
     return view('users.index')->with('users',User::all());
    }
    /**
     * 
     * handle edit profile
     * 
     */
    public function edit(){
     return view('users.edit')->with('user', auth()->user());
    }
    public function update(UpdateProfileRequest $request){
     $user=auth()->user();
     $user->update([
        'name'=>$request->name,
        'about'=>$request->about
     ]);
     session()->flash('success','User Update successfully');
     return redirect()->back();
    }
    /**
     * 
     * hanle make admin
     * 
     */
    public function makeAdmin(User $user){
     $user->role='admin';
     $user->save();
     session()->flash('success','User made Admin successfully');
     return redirect(route('users.index'));
    }
}
